package com.sda.media;

import java.time.Duration;

public class Video extends MediaLength {

    VideoType type;

    public Video(String name, Duration length, VideoType type) {
        super(name, length);

        this.type = type;
    }

    @Override
    public String toString() {
        return "Video{" +
                "type=" + type +
                super.toString() +
                '}';
    }
}
