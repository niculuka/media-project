package com.sda.media;

import java.time.Duration;

public class MediaLength extends Media{

    private Duration length;

    public MediaLength(String name, Duration length) {
        super(name);

        this.length = length;


    }

    @Override
    public String toString() {
        return "MediaLength{" +
                "length=" + length +
                super.toString() +
                '}';
    }
}
